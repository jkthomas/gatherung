using Microsoft.AspNetCore.Mvc;
using Database;

namespace gatherung.Posts
{
  [ApiController]
  [Route("posts")]
  public class PostsController : ControllerBase
  {

    private readonly ILogger<PostsController> _logger;
    private readonly GatherungContext _context;

    public PostsController(ILogger<PostsController> logger, GatherungContext context)
    {
      _logger = logger;
      _context = context;
    }

    [HttpGet(Name = "GetPosts")]
    public IEnumerable<Post> Get()
    {
      return _context.Posts.ToList();
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Post))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult GetById(int id)
    {
      var post = _context.Find(typeof(Post), id);
      if (post == null)
      {
        return NotFound();
      }

      return Ok(post);
    }

    [HttpPost]
    public async Task<ActionResult<Post>> Post(Post post)
    {
      post.Id = _context.Posts.Any() ?
             _context.Posts.Max(p => p.Id) + 1 : 1;
      _context.Posts.Add(post);
      await _context.SaveChangesAsync();

      return CreatedAtAction(nameof(GetById), new { id = post.Id }, post);
    }
  }
}
