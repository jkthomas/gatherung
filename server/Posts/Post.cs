namespace gatherung.Posts
{
  public class Post
  {
    // public DateTime DateCreated { get; set; }
    // public DateTime DateUpdated { get; set; }
    public int? Id { get; set; }
    public String? AuthorName { get; set; }
    public String? Title { get; set; }
    public String? Description { get; set; }
    public String? Content { get; set; }
  }
}
