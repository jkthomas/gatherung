namespace gatherung.Users
{
  public class User
  {
    public int? Id { get; set; }
    public String? Username { get; set; }
    public String? Password { get; set; }
    public Byte[]? PasswordSalt { get; set; } // TODO: Move out of the same table/perfectly DB
    public DateTime DateCreated { get; set; }
  }
}
