namespace gatherung.Users.Hashing
{
  public class HashData
  {
    public string? HashedString { get; set; }
    public byte[]? Salt { get; set; }

    public void Deconstruct(out string? hash, out byte[]? salt)
    {
      hash = HashedString;
      salt = Salt;
    }
  }
}