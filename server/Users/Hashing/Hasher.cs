
using System.Security.Cryptography;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;

namespace gatherung.Users.Hashing
{
  public class Hasher
  {
    private string HashString(string inputString, byte[] salt)
    {
      return Convert.ToBase64String(KeyDerivation.Pbkdf2(
        password: inputString,
        salt: salt,
        prf: KeyDerivationPrf.HMACSHA256,
        iterationCount: 10000,
        numBytesRequested: 256 / 8
      ));
    }

    public HashData GetHashedString(string inputString)
    {
      byte[] salt = new byte[128 / 8];
      RandomNumberGenerator.Create().GetBytes(salt);
      string hashedString = this.HashString(inputString, salt);
      return new HashData { HashedString = hashedString, Salt = salt };
    }

    public bool Verify(string inputString, byte[] salt, string hashedString)
    {
      string encryptedPassw = this.HashString(inputString, salt);
      return encryptedPassw == hashedString;
    }
  }
}