using Microsoft.AspNetCore.Mvc;
using Database;
using gatherung.Users.Hashing;

namespace gatherung.Users
{
  [ApiController]
  [Route("users")]
  public class UsersController : ControllerBase
  {
    // TODO: Add services
    // TODO: Add DTOs, return specific fields only
    // TODO: Add authorisation and authentication
    // TODO: Create better nullchecks
    private readonly ILogger<UsersController> _logger;
    private readonly GatherungContext _context;
    private readonly Hasher _hasher;

    public UsersController(ILogger<UsersController> logger, GatherungContext context)
    {
      _logger = logger;
      _context = context;
      _hasher = new Hasher();
    }

    [HttpGet(Name = "GetUsers")]
    public IEnumerable<User> GetAll()
    {
      return _context.Users.ToList();
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(User))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult GetById(int id)
    {
      var user = _context.Find(typeof(User), id);
      if (user == null)
      {
        return NotFound();
      }

      return Ok(user);
    }

    [HttpPost]
    public async Task<ActionResult<User>> Save(User user)
    {
      user.Id = _context.Users.Any() ?
             _context.Users.Max(p => p.Id) + 1 : 1;
      (user.Password, user.PasswordSalt) = this._hasher.GetHashedString(user.Password);
      _context.Users.Add(user);
      await _context.SaveChangesAsync();

      return CreatedAtAction(nameof(GetById), new { id = user.Id }, user);
    }

    [HttpPost("/login")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    public ActionResult<User> Login(LoginData loginData)
    {
      if (loginData == null || loginData.Password == null)
      {
        return BadRequest(new { message = "Missing login data" });
      }

      User? user = _context.Users.FirstOrDefault(u => u.Username == loginData.Username);

      if (user == null)
      {
        return BadRequest(new { message = "Username or password are incorrect" });
      }

      bool isPasswordCorrect = this._hasher.Verify(loginData.Password, user.PasswordSalt, user.Password);

      if (!isPasswordCorrect)
      {
        return BadRequest(new { message = "Username or password are incorrect" });
      }

      return Ok(new { message = "Login successful" });
    }
  }
}
