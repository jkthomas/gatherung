using Microsoft.AspNetCore.Mvc;
using Database;

namespace gatherung.Suggestions
{
  [ApiController]
  [Route("suggestions")]
  public class SuggestionsController : ControllerBase
  {

    private readonly ILogger<SuggestionsController> _logger;
    private readonly GatherungContext _context;

    public SuggestionsController(ILogger<SuggestionsController> logger, GatherungContext context)
    {
      _logger = logger;
      _context = context;
    }

    [HttpGet(Name = "GetSuggestions")]
    public IEnumerable<Suggestion> GetAll()
    {
      return _context.Suggestions.ToList();
    }

    [HttpGet("{id}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(Suggestion))]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    public IActionResult GetById(int id)
    {
      var suggestion = _context.Find(typeof(Suggestion), id);
      if (suggestion == null)
      {
        return NotFound();
      }

      return Ok(suggestion);
    }

    [HttpPost]
    public async Task<ActionResult<Suggestion>> Save(Suggestion suggestion)
    {
      suggestion.Id = _context.Suggestions.Any() ?
             _context.Suggestions.Max(p => p.Id) + 1 : 1;
      _context.Suggestions.Add(suggestion);
      await _context.SaveChangesAsync();

      return CreatedAtAction(nameof(GetById), new { id = suggestion.Id }, suggestion);
    }
  }
}
