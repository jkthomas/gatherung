namespace gatherung.Suggestions
{
  public class Suggestion
  {
    public int? Id { get; set; }
    public String? AuthorName { get; set; }
    public String? Title { get; set; }
    public String? Link { get; set; }
    public DateTime DateCreated { get; set; }
    public DateTime DateUpdated { get; set; }
  }
}
