using Microsoft.EntityFrameworkCore;
using gatherung.Posts;
using gatherung.Suggestions;
using gatherung.Users;

namespace Database
{
  public class GatherungContext : DbContext
  {
    public GatherungContext(DbContextOptions<GatherungContext> options)
        : base(options)
    {
    }

    public DbSet<User> Users { get; set; } = null!;
    public DbSet<Post> Posts { get; set; } = null!;
    public DbSet<Suggestion> Suggestions { get; set; } = null!;
  }
}